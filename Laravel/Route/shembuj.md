## Route me slugs
<pre>
Route::get('{page}', array('uses' => 'PageController@show'));
</pre>
<pre>
Route::group(array('prefix' => 'admin'), function() 
{ 
Route::get('{slug}', ['uses' => 'AdminController@show']);
});
</pre>